import { Injectable } from "@angular/core";
import { Alert } from "../modelo/alert.model";

@Injectable()
export class NgbdAlertCloseable {
	alert: Alert;

	constructor() {}

  public add(alert: Alert, type: string, message: string){
    alert.type = type;
    alert.message = message;
  }

	public close(alert: Alert) {
		alert.message = '';
	}

}
