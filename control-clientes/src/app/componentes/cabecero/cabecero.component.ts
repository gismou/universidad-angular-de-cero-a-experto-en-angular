import { Router } from '@angular/router';
import { LoginService } from './../../servicios/login.service';
import { Component, OnInit } from '@angular/core';
import { ConfiguracionServicio } from 'src/app/servicios/configuracion.service';

@Component({
  selector: 'app-cabecero',
  templateUrl: './cabecero.component.html',
  styleUrls: ['./cabecero.component.css']
})
export class CabeceroComponent implements OnInit {
  isMenuCollapsed = true;
  isMenuBienvenidoCollapsed = true;
  isLoggedIn: boolean;
  loggedInUser: string;
  permitirRegistro: boolean;

  constructor( private LoginService: LoginService,
               private router: Router,
               private configuracionServicio: ConfiguracionServicio){};

  ngOnInit(): void {
    this.LoginService.getAuth().subscribe( auth => {
      if(auth){
        this.loggedInUser = auth.email;
        this.isLoggedIn = true;
      }else{
        this.isLoggedIn = false;
      }
    });

    this.configuracionServicio.getConfiguracion().subscribe( configuracion => {
      this.permitirRegistro = configuracion.permitirRegistro;
    })
  }

  logout(){
    this.LoginService.logout();
    this.isLoggedIn = false;
    this.router.navigate(["/login"])
  };
}
