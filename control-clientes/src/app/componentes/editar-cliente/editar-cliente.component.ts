import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Alert } from 'src/app/modelo/alert.model';
import { Cliente } from 'src/app/modelo/cliente.model';
import { NgbdAlertCloseable } from 'src/app/servicios/alerts.service';
import { ClienteServicio } from 'src/app/servicios/cliente.service';

@Component({
  selector: 'app-editar-cliente',
  templateUrl: './editar-cliente.component.html',
  styleUrls: ['./editar-cliente.component.css']
})
export class EditarClienteComponent implements OnInit {

  hide = true;
  id: string;
  alert: Alert = {
    type: '',
    message: ''
  };
  closeResult = '';
  cliente: Cliente = {
    nombre: '',
    apellido: '',
    email: '',
    saldo: 0
  }

  constructor(private clienteServicio: ClienteServicio,
              private router: Router,
              private route: ActivatedRoute,
              private ngdbAlertCloseable: NgbdAlertCloseable
             ) {}

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.clienteServicio.getCliente(this.id).subscribe( cliente => {
      this.cliente = cliente;
    });
  }

  guardar({value,valid}: {value: Cliente, valid: boolean}){
    if(!valid){
      this.ngdbAlertCloseable.add(this.alert,'danger','Por favor rellene el formulario correctamente');
      console.log(valid);
    }else{
      value.id = this.id;
      // Modificamos el cliente
      this.clienteServicio.modificarCliente(value);
      this.router.navigate(['/']);
    }
  }

  eliminar(){
    if(confirm('¿Seguro que desea eliminar el cliente?')){
      console.log(this.cliente);
      this.clienteServicio.eliminarCliente(this.cliente);
      this.router.navigate(['/']);
    }
  }

  // Cerrar alert
  close(alert: Alert) {
		this.ngdbAlertCloseable.close(alert);
	}

}
