import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Alert } from 'src/app/modelo/alert.model';
import { NgbdAlertCloseable } from 'src/app/servicios/alerts.service';
import { LoginService } from 'src/app/servicios/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit{

  alert: Alert = {
    type: '',
    message: ''
  };
  closeResult = '';
  email: string;
  password: string;

  constructor( private ngdbAlertCloseable: NgbdAlertCloseable,
               private router: Router,
               private loginService: LoginService) {}

  ngOnInit(): void {
    this.loginService.getAuth().subscribe( auth => {
      if(auth){
        this.router.navigate(['/']);
      }
    })
  }

   // Cerrar alert
  close(alert: Alert) {
		this.ngdbAlertCloseable.close(alert);
	}

  login(){
    this.loginService.login(this.email, this.password)
      .then( res => {
        this.router.navigate(['/']);
      })
      .catch(error => {
        this.ngdbAlertCloseable.add(this.alert,'danger',error.message);
      })
  };

}
