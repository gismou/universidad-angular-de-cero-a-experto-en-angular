import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Cliente } from 'src/app/modelo/cliente.model';
import { ClienteServicio } from 'src/app/servicios/cliente.service';
import { NgbdAlertCloseable } from 'src/app/servicios/alerts.service';
import { Alert } from 'src/app/modelo/alert.model';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {

  alert: Alert = {
    type: '',
    message: ''
  };
  closeResult = '';
  clientes: Cliente[];
  cliente: Cliente = {
    nombre: '',
    apellido: '',
    email: '',
    saldo: 0
  }
  hide = true;

  @ViewChild("clienteForm") clienteForm: NgForm;

  constructor(private clienteServicio: ClienteServicio,
              private modalService: NgbModal,
              private ngdbAlertCloseable: NgbdAlertCloseable
             ) {}

  ngOnInit() {
    this.clienteServicio.getClientes().subscribe(
      clientes => {
        this.clientes = clientes;
      }
    )
  }

  open(content: any) {
		this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
			(result) => {
				this.closeResult = `Closed with: ${result}`;
			},
			(reason) => {
				this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
			},
		);
	}

	private getDismissReason(reason: any): string {
		if (reason === ModalDismissReasons.ESC) {
			return 'by pressing ESC';
		} else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
			return 'by clicking on a backdrop';
		} else {
			return `with: ${reason}`;
		}
	}

  getSaldoTotal() {
    let saldoTotal: number = 0;
    if(this.clientes){
      this.clientes.forEach(cliente => {
        saldoTotal += cliente.saldo;
      })
    }
    return saldoTotal;
  }

  agregar({value,valid}: {value: Cliente, valid: boolean}, modal){
    if(!valid){
      this.ngdbAlertCloseable.add(this.alert,'danger','Por favor rellene el formulario correctamente');
      console.log(valid);
    }else{
      // Agregar el nuevo cliente, resetar formulario y cerrar modal
      this.clienteServicio.agregarCliente(value);
      this.clienteForm.resetForm();
      modal.dismiss('Cross click');
    }
  }

  // Cerrar alert
  close(alert: Alert) {
		this.ngdbAlertCloseable.close(alert);
	}

}
