/* OPERADORES ARITMÉTICOS ---------------- */

/* let a = 3, b = 2;
let z = a + b; */
/* console.log("Resultado de la suma: " + z);

z = a - b;
console.log("Resultado de la resta: " + z);

z = a * b;
console.log("Resultado de la multiplicación: "+ z);

z = a / b;
console.log("Resultado de la división: "+ z);

z = a % b;
console.log("Resultado de operador modulo (resto): "+z);

z = a ** b;
console.log("Resultado de operador exponente: "+z); */

/* INCREMENTOS ----------------- */

// Pre-incremento (el operador ++ antes de la variable)
/* z = ++a;
console.log(a);
console.log(z); */

// Post-incremento (el operador ++ después de la variable)
/* z = b++;
console.log(b);
console.log(z); */

/* DECREMENTOS ----------------- */

// Predecremento
/* z = --a;
console.log(a);
console.log(z); */

// Postdecremento
/* z = b--;
console.log(b);
console.log(z); */

/* PRECEDENCIA DE OPERADORES ------------- */

/* let a = 3, b = 2, c = 1, d = 4; */

// Se lee de izquierda a derecha
/* let z = a * b + c;
console.log(z); */

// Hay prioridades de operaciones, la multiplicación tiene mas prioridad que la suma
/* z = c + a * b;
console.log(z); */

// Mezcla de operadores con prioridad y lectura de izquierda a derecha
/* z = a * b + c / d;
console.log(z); */

/* z = c + a * b / d;
console.log(z); */

// El parentesis tiene la mayor prioridad
/* z = (c + a) * b / c;
console.log(z); */

/* OPERADORES DE ASIGNACIÓN --------------- */

// básico
/* let a = 1;
console.log(a); */

// compuesto con incremento
/* a += 3;
console.log(a); */

// compuesto con decremento
/* a -= 2;
console.log(a); */

// compuesto con multiplicación
/* a *= 2;
console.log(a); */

// compuesto con división
/* a /= 2; */
/* console.log(a); */

// compuesto con resto
/* a %= 1;
console.log(a); */

/* OPERADORES DE COMPARACIÓN ------------------ */

/* let a = 3, b = 2, c = "3"; */

// compara si son iguales y devuelve true si lo es y false si no lo es (no estricto)
/* let z = a == b;
console.log(z); */

// compara si son estrictamente iguales (incluye los tipos de dato también)
/* z = a === c; */
/* console.log(z);  */// false, son diferentes tipos y es estricto

/* z = a == c; */
/* console.log(z);  */// true, es diferente tipo, pero el comparador de igualdad no es estricto

// compara si son distintos
/* z = a != c;  */// no es estricto, no mira tipos
/* console.log(z); */

/* z = a !== c;  */// si es estricto, mira los tipos
/* console.log(z); */

/* OPERADORES RELACIONALES --------------- */

/* let a = 3, b = 3, c = "3"; */

// menor que
/* let z = a < b;
console.log(z);

// menor o igual que
z = a <= b;
console.log(z);

// mayor que
z = a > b;
console.log(z);

// mayor o igual que
z = a >= b;
console.log(z); */

// SABER SI UN NÚMERO ES PAR ------------ */

/* let a = 9;

if ( a % 2  == 0){
    console.log("Es un número par");
}else{
    console.log("Es un número impar");
}; */

// SABER SI ES MAYOR DE EDAD ------------ */

/* let edad = 17, adulto = 18;

if ( edad >= adulto ){
    console.log('Es mayor de edad');
}else{
    console.log('No es mayor de edad');
}; */

/* OPEADOR AND Y OR ----------------- */

// and (&&)

/* let a = 15, valMin = 0, valMax = 10;

if ( a >= valMin && a <= valMax ){
    console.log("dentro de rango");
}else{
    console.log("fuera de rango");
}; */

// or (||)

/* let vacaciones = true, diaDescanso = false;

if ( vacaciones || diaDescanso) {
    console.log('Puede asistir');
}else{
    console.log('No puede asistir');
}
 */

/* OPERADOR TERNARIO ---------------------- */

// el primer valor tras el ? es lo que devuelve si es cierto, lo segundo es lo que devuelve si es falso.
/* let resultado = (1 > 2) ? "verdadero" : "falso";
console.log(resultado);

let numero = 10;
resultado = ( numero % 2 == 0 ) ? "es par" : "es impar";
console.log(resultado); */

/* CONVERTIR STRING A NUMBER ------------------ */

/* let miNumero = "10";

console.log(miNumero);
console.log(typeof miNumero);

// función Number convierte a número
let edad = Number(miNumero);

console.log(edad);
console.log(typeof edad);

if (edad >= 18){
    console.log("puede votar");
}else{
    console.log("no puede votar");
}

let puedeVotar = (edad >= 18) ? true : false;
console.log(puedeVotar);

edad = 18;

puedeVotar = (edad >= 18) ? true : false;
console.log(puedeVotar); */

/* FUNCIÓN ISNAN ------------------------------ */

let miNumero2 = "18x";

let miEdad = Number(miNumero2);
console.log(miEdad); // es un NaN (not a number)

if (isNaN(miEdad)){
    console.log("No es un número")
}else{
    console.log("Si es un número");
}

/* EJEMPLOS MAS COMPLEJOS DE PRECEDENCIA DE OPERADORES ------------------------ */

// ej.1
let x = 5;
let y = 10;
let z = ++x + y--;

console.log(x);
console.log(y);
console.log(z);

// ej.2
let resultado = 4 + 5 * 6 / 3; 
console.log(resultado);