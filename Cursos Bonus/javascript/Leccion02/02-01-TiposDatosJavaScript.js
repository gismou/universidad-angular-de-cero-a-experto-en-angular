/* Ejemplos de tipos de datos en JavaScript */

// Tipo de dato string
/* let nombre = "Pedro";
console.log(nombre); */

/* nombre = 10;
console.log(typeof nombre) */

// Tipo de datos numerico
/* let numero = 1000;
console.log(numero); */

// Tipo de dato object
/* let object = {
    nombre: 'Juan',
    apellido: 'Perez',
    telefono: 55443322
} */
/* console.log(typeof object); */
 
// Tipo de objeto boolean (true, false)
/* let bandera = false;
console.log(typeof bandera); */

// Tipo de dato function
/* function miFuncion(){};
console.log(typeof miFuncion) */

// Tipo de dato Symbol
/* let simbolo = Symbol("mi simbolo");
console.log(typeof simbolo) */

// Tipo clase es una function
/* class Persona{
    constructor(nombre, apellido){
        this.nombre = nombre;
        this.apellido = apellido;
    }
}
console.log(typeof Persona) */

// Tipo undefined
/* let x;
console.log(typeof x); */

/* x = undefined;
console.log(typeof x); */

// null = ausencia de valor
/* let y = null;
console.log(typeof y); */

// array / arreglo
/* let autos = ['BMW','Audi','Volvo'];
console.log(typeof autos); */

// cadenas vacias
/* let z = '';
console.log(typeof z); */

/* CONCATENAR CADENAS */

/* let nombre = 'Juan';
let apellidos = 'Perez';
console.log(nombre + " " + apellidos);

let nombreCompleto = 'Juan' + ' ' + 'Perez';
console.log(nombreCompleto)

let x = nombre + 2 + 4;
console.log(x);

x = nombre + (2 + 4);
console.log(x);

x = 2 + 4 + nombre;
console.log(x); */

/* DECLARACIÓN DE VARIABLES */
// var está deprecated

// declaración sin palabra reservadad
nombre = 'Juana'; // no es buena practica no darle let o const
console.log(nombre);

// declaración de variable con palabra reservada let
let nombre2 = 'Juanita';
console.log(nombre2);

// declaracion de variable como constante con palabra reservada const
const apellido = 'Perez';
apellido = 'Lara'; // no permite cambiar el valor de la variable porque es constante
console.log(apellido);
