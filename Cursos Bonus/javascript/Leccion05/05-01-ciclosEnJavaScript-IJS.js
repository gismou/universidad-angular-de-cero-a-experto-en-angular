/* CICLO WHILE ---------------------------- */

/* let contador = 0;

while(contador < 3){
    console.log(contador);
    contador ++;
} */

/* CICLO DO WHILE --------------------------- */

/* let contador = 0;

do{
    console.log(contador);
    contador ++;
}while(contador < 3); */

/* CICLO FOR -------------------------------- */

/* for(let i = 0; i < 3; i++){
    console.log(i);
} */

/* USO DE LA PALABRA BREAK PARA ROMPER CICLOS --------------- */

/* for(let contador = 0; contador <= 10; contador++ ){
    if(contador % 2 == 0){
        console.log(contador);
        break;
    }
}


/* USO DE LA PALABRA CONTINUE EN CICLOS ---------------------- */

/* for(let contador = 0; contador <= 10; contador++ ){
    if(contador % 2 !== 0){
        continue; // ir a la siguiente iteración
    }
    console.log(contador);
} */

/* USO DE ETIQUETAS EN CICLOS (NO RECOMENDABLE) -------------- */

inicio:
for(let contador = 0; contador <= 10; contador++ ){
    if(contador % 2 !== 0){
        continue inicio // ir a la siguiente iteración
    }
    console.log(contador);
}
