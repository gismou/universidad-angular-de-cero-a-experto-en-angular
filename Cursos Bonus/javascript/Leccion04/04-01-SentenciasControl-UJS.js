/* SENTENCIA IF/ELSE ---------------------- */

// ejemplo básico de if/else
/* if (2>3){
    console.log("Es verdadera");
}  else{
    console.log("Es falsa");
} 
    
let numero = 2; */

// ejemplo de if/else con else if
/* if(numero == 1){
    console.log("Número uno");
}else if(numero == 2){
    console.log("Número dos");
}else if(numero == 3){
    console.log("Número tres");
}else if(numero == 4){
    console.log("Número cuatro");
}else{
    console.log("Número desconocido");
} */

// ejemplo de calculo de estacion del año

/* let mes = 6;
let estacion;

if (mes == 1 || mes == 2 || mes == 12){
    estacion = "Invierno";
}else if(mes == 3 || mes == 4 || mes == 5){
    estacion = "Primavera";
}else if(mes == 6 || mes == 7 || mes == 8){
    estacion = "Verano";
}else if(mes == 9 || mes == 10 || mes == 11){
    estacion = "Otoño";
}else{
    estacion = "Mes incorrecto";
}

console.log(estacion); */

// ejemplo de saludo según hora

/*
6am-11am - Buenos días
12pm-18pm - Buenas tardes
19pm-24pm - Buenas noches
0am-6am - ZzZzZzz
*/

/* let hora = 5;
let saludo;

if (hora >= 6 && hora <= 11){
    saludo = "Buenos días";
}else if (hora >= 12 && hora <= 18){
    saludo = "Buenas tardes";
}else if (hora >= 19 && hora <= 24){
    saludo = "Buenas noches";
}else if (hora >= 0 && hora < 6){
    saludo = "ZzZzZzz"
}else{
    saludo = "Hora incorrecta, debe ser un número entre 0 y 24";
}

console.log(saludo); */

/* ESTRUCUTRA SWITCH -------------------- */

// caso básico de switch
let numero = 3;
let numeroTexto;

switch(numero){
    case 1:
        numeroTexto = 'Número 1';
        break;
    case 2:
        numeroTexto = 'Número 2';
        break;
    case 3:
        numeroTexto = 'Número 3';
        break;
    case 4:
        numeroTexto = 'Número 4';
        break;
    default:
        numeroTexto = 'Caso no encontrado';
}

console.log(numeroTexto);

// ejemplo de determinar el año 
let mes = 4;
let estacion;

switch(mes){
    case 1: case 2: case 12:
        estacion = 'Invierno';
        break;
    case 3: case 4: case 5:
        estacion = 'Primavera';
        break;
    case 6: case 7: case 8:
        estacion = 'Verano';
        break;
    case 9: case 10: case 11:
        estacion = 'Otoño';
        break;
    default:
        estacion = 'Mes no válido';
}

console.log(estacion);
