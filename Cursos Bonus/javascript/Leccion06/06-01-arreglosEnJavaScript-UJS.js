// ARRAYS -----------------------

/* let autos = new Array('BMW','Mercedes Benz','Volvo') */ // forma antigua de declarar array
const autos = ['BMW','Mercedes Benz','Volvo']; // aunque se pone constante, lo que es constante es la posición de memoria del array, no el contenido de la misma que puede variar
console.log(autos);
autos.push('Seat');
console.log(autos); 

// RECORRER ELEMENTOS DE ARRAYS --------------------

console.log(autos[1]);

for(let i = 0; i < autos.length; i++){
    console.log(i + ' : ' + autos[i]);
}

// MODIFICAR LOS ELEMENTOS DE UN ARRAY ----------------

autos[1] = 'MercedesBenz';
console.log(autos[1]);


// AGREGAR ELEMENTOS A UN ARRAY -------------------------

// con .push
autos.push('Audi');
console.log(autos);

// con .length
console.log(autos.length) // número de elementos en el array;
autos[autos.length] = 'Cadillac'; // usando el .length nos aseguramos de poner el nuevo elemento en la posición siguiente
console.log(autos);
autos[8] = 'Bentley'; // podemos agregar a una posición fija que aún no existe (como si modificaramos), pero eso creará huecos vacios si las anteriores no tienen valores
console.log(autos);

// COMO SABER SI ES UN ARRAY ----------------------------

// con la función isArray de la clase Array
console.log(Array.isArray(autos));

// con instanceof de la clase Array
console.log(autos instanceof Array);

// BORRAR ELEMENTOS DE UN ARRAY SIN DEJAR HUECOS ----------------------

autos.splice(6,2); // primer parámetro es de que posición empieza a borrar, y el segundo cuantos borra
console.log(autos);