// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCVVDxV7ja4iGGtuZ0x9TMqQfX-_kVWwEE",
    authDomain: "listado-personas-884d8.firebaseapp.com",
    databaseURL: "https://listado-personas-884d8-default-rtdb.firebaseio.com",
    projectId: "listado-personas-884d8",
    storageBucket: "listado-personas-884d8.appspot.com",
    messagingSenderId: "58260878989",
    appId: "1:58260878989:web:b8ca94e5705a291fdbd4c4",
    measurementId: "G-TEQ3SPTC3Z"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
