import { LoggingService } from "./LoggingService.service";
import { DataServices } from "./data.services";
import { Persona } from "./persona.model";
import { EventEmitter, Injectable } from "@angular/core";

@Injectable()
export class PersonasService {

  personas: Persona[] = [];

  saludar = new EventEmitter<number>();

  constructor(private loggingService: LoggingService,
              private dataServices: DataServices){};

  setPersonas(personas: Persona[]){
    this.personas = personas;
  }

  obtenerPersonas(){
    return this.dataServices.cargarPersonas();
  }

  personaAgregada(persona: Persona){
    this.loggingService.enviaMensajeAConsola("Agregamos persona: "+persona.nombre+" "+persona.apellido);
    if(this.personas == null) this.personas = []
    else
    this.personas.push(persona);
    this.dataServices.guardarPersonas(this.personas);
  }

  encontrarPersona(index: number){
    let persona: Persona = this.personas[index];
    return persona;
  }

  modificarPersona(index: number, persona: Persona){
    let personaModificar = this.personas[index];
    personaModificar.nombre = persona.nombre;
    personaModificar.apellido = persona.apellido;
    this.dataServices.modificarPersona(index, persona);
  }

  eliminarPersona(index: number){
    this.personas.splice(index,1);
    this.dataServices.eliminarPersona(index);
    // Se vuelve a guardar el arreglo personas para regenerar los indices con huecos
    this.regenerarIndices();
  }

  regenerarIndices(){
    if(this.personas != null) this.dataServices.guardarPersonas(this.personas);
  }

}
