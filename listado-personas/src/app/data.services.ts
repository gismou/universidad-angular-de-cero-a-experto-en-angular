import { LoginService } from './login/login.service';
import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core';
import { Persona } from './persona.model';

@Injectable()
export class DataServices{
  constructor(private httpClient: HttpClient,
              private loginService: LoginService){}

  private urlDB = "https://listado-personas-884d8-default-rtdb.firebaseio.com";

  cargarPersonas(){
    const token = this.loginService.getIdToken();
    return this.httpClient.get<Persona[]>(this.urlDB+'/datos.json?auth='+token);
  }

  guardarPersonas(personas: Persona[]){
    const token = this.loginService.getIdToken();
    this.httpClient.put(this.urlDB+'/datos.json?auth=' + token, personas)
    .subscribe(
      response => {
        console.log("resultado de guardar Personas: " + response)
      },
      error => console.log("Error al guardar Personas: " +error)
    )
  }

  modificarPersona(index: number, persona: Persona){
    const token = this.loginService.getIdToken();
    let url: string;
    url =  this.urlDB + '/datos/' + index + '.json?auth=' + token;
    this.httpClient.put(url,persona)
        .subscribe(
          response => console.log("resultado modifiar: " + response),
          error => console.log("Error en modificar Persona: " + error)
        )
  }

  eliminarPersona(index: number){
    const token = this.loginService.getIdToken();
    let url: string;
    url = this.urlDB + '/datos/' + index + '.json?auth' + token;
    this.httpClient.delete(url)
        .subscribe(
          response => console.log("resultado de eliminar Persona " + response),
          error => console.log("Error en eliminar Persona " + error)
        )
  }
}
