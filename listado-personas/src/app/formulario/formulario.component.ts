import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Persona } from '../persona.model';
import { LoggingService } from '../LoggingService.service';
import { PersonasService } from '../personas.service';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

  nombreInput: string  = '';
  apellidoInput: string = '';
  index: number;
  modoEdicion: number;

  constructor(private loggingService: LoggingService,
              private personaService: PersonasService,
              private router: Router,
              private route: ActivatedRoute)
  {
      this.personaService.saludar.subscribe(
        (indice: number) => alert("El indice es " + indice)
      );
  };

  ngOnInit() {
    this.index = this.route.snapshot.params['id'];
    this.modoEdicion = +this.route.snapshot.queryParams['modoEdicion'];

    if(this.modoEdicion != null && this.modoEdicion === 1){
      let persona: Persona = this.personaService.encontrarPersona(this.index);
      this.nombreInput = persona.nombre;
      this.apellidoInput = persona.apellido;
    }
  }

  onGuardarPersona(){
    let personaNueva = new Persona(this.nombreInput, this.apellidoInput)
    if(this.modoEdicion != null && this.modoEdicion === 1){
      this.personaService.modificarPersona(this.index, personaNueva);
    }else{
      this.personaService.personaAgregada(personaNueva);
    }
    this.router.navigate(['personas']);
  };


  eliminarPersona(){
    if(this.index != null){
      this.personaService.eliminarPersona(this.index);
    }
    this.router.navigate(["personas"]);
  }

}
