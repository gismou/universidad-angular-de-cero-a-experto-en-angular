import { Component, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-calculadora',
  templateUrl: './calculadora.component.html',
  styleUrls: ['./calculadora.component.css']
})
export class CalculadoraComponent implements OnInit {

  @Input() resultadoPadre: number = 0;

  titulo: string = 'Aplicación de Calculadora';

  constructor() { }

  ngOnInit(): void {
  }

 getResultado(resultado: number){
  this.resultadoPadre = resultado;
 }

}
