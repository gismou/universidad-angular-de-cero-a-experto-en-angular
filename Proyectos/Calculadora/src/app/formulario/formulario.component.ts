import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

  @Output() resultadoOperacion = new EventEmitter<number>();

  operandoa: number = 0;
  operandob: number = 0;

  constructor() { }

  ngOnInit(): void {
  }

  sumar(): void{
    this.resultadoOperacion.emit(this.operandoa + this.operandob);
  }

  restar(): void{
    this.resultadoOperacion.emit(this.operandoa - this.operandob);
  }

  multiplicar(): void{
    this.resultadoOperacion.emit(this.operandoa * this.operandob);
  }

  dividir(): void{
    this.resultadoOperacion.emit(this.operandoa / this.operandob);
  }

}
